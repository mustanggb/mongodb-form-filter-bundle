<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Event;

use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Query\QueryInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Get alias and expression builder for filter builder
 */
class PrepareEvent extends Event
{
    /**
     * @var object $queryBuilder
     */
    private $queryBuilder;

    /**
     * @var object $filterQuery
     */
    private $filterQuery;

    /**
     * Construct
     *
     * @param object $queryBuilder
     */
    public function __construct($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * Get query builder
     *
     * @return object
     */
    public function getQueryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     * Set filter query
     *
     * @param QueryInterface $filterQuery
     */
    public function setFilterQuery(QueryInterface $filterQuery)
    {
        $this->filterQuery = $filterQuery;
    }

    /**
     * Get filter query
     *
     * @return QueryInterface
     */
    public function getFilterQuery()
    {
        return $this->filterQuery;
    }
}
