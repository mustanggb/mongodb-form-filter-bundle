<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Event\Subscriber;

use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\QueryBuilder;
use MustangGB\Bundle\MongoDBFormFilterBundle\Event\GetFilterConditionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Register listeners to compute conditions to be applied on a Doctrine ORM query builder.
 */
class DoctrineORMSubscriber extends AbstractDoctrineSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // mustanggb form filter types
            'mustanggb_mongodb_form_filter.apply.orm.filter_boolean' => ['filterBoolean'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_checkbox' => ['filterCheckbox'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_choice' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_date' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_date_range' => ['filterDateRange'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_datetime' => ['filterDateTime'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_datetime_range' => ['filterDateTimeRange'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_entity' => ['filterEntity'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_number' => ['filterNumber'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_number_range' => ['filterNumberRange'],
            'mustanggb_mongodb_form_filter.apply.orm.filter_text' => ['filterText'],
            // Symfony types
            'mustanggb_mongodb_form_filter.apply.orm.text' => ['filterText'],
            'mustanggb_mongodb_form_filter.apply.orm.email' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.integer' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.money' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.number' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.percent' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.search' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.url' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.choice' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.entity' => ['filterEntity'],
            'mustanggb_mongodb_form_filter.apply.orm.country' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.language' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.locale' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.timezone' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.date' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.orm.datetime' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.orm.birthday' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.orm.checkbox' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.orm.radio' => ['filterValue'],
        ];
    }

    /**
     * @param GetFilterConditionEvent $event
     * @throws \Exception
     */
    public function filterEntity(GetFilterConditionEvent $event)
    {
        $expr = $event->getFilterQuery()->getExpr();
        $values = $event->getValues();

        if (is_object($values['value'])) {
            $paramName = $this->generateParameterName($event->getField());
            $filterField = $event->getField();

            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = $event->getQueryBuilder();

            if ($dqlFrom = $event->getQueryBuilder()->getDQLPart('from')) {
                $rootPart = reset($dqlFrom);
                $fieldName = ltrim($event->getField(), $rootPart->getAlias() . '.');
                $metadata = $queryBuilder->getEntityManager()->getClassMetadata($rootPart->getFrom());

                if (isset($metadata->associationMappings[$fieldName]) && (!$metadata->associationMappings[$fieldName]['isOwningSide'] || $metadata->associationMappings[$fieldName]['type'] === ClassMetadataInfo::MANY_TO_MANY)) {
                    if (!$event->getFilterQuery()->hasJoinAlias($fieldName)) {
                        $queryBuilder->leftJoin($event->getField(), $fieldName);
                    }

                    $filterField = $fieldName;
                }
            }

            if ($values['value'] instanceof Collection) {
                $ids = [];

                foreach ($values['value'] as $value) {
                    $ids[] = $this->getEntityIdentifier($value, $queryBuilder->getEntityManager());
                }

                if (count($ids) > 0) {
                    $event->setCondition(
                        $expr->in($filterField, ':' . $paramName),
                        [$paramName => [$ids, Connection::PARAM_INT_ARRAY]]
                    );
                }
            } else {
                $event->setCondition(
                    $expr->eq($filterField, ':' . $paramName),
                    [$paramName => [$this->getEntityIdentifier($values['value'], $queryBuilder->getEntityManager()), Types::INTEGER]]
                );
            }
        }
    }

    /**
     * @param object $value
     * @return integer
     * @throws \RuntimeException
     */
    protected function getEntityIdentifier($value, EntityManagerInterface $em)
    {
        $class = get_class($value);
        $metadata = $em->getClassMetadata($class);

        if ($metadata->isIdentifierComposite) {
            throw new \RuntimeException(sprintf('Composite identifier is not supported by FilterEntityType.', $class));
        }

        $identifierValues = $metadata->getIdentifierValues($value);

        if (empty($identifierValues)) {
            throw new \RuntimeException(sprintf('Can\'t get identifier value for class "%s".', $class));
        }

        return array_shift($identifierValues);
    }
}
