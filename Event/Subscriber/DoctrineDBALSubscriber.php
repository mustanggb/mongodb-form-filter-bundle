<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Event\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *  Register listeners to compute conditions to be applied on a Doctrine DBAL query builder.
 */
class DoctrineDBALSubscriber extends AbstractDoctrineSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // mustanggb form filter types
            'mustanggb_mongodb_form_filter.apply.dbal.filter_boolean' => ['filterBoolean'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_checkbox' => ['filterCheckbox'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_choice' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_date' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_date_range' => ['filterDateRange'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_datetime' => ['filterDateTime'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_datetime_range' => ['filterDateTimeRange'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_number' => ['filterNumber'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_number_range' => ['filterNumberRange'],
            'mustanggb_mongodb_form_filter.apply.dbal.filter_text' => ['filterText'],
            // Symfony field types
            'mustanggb_mongodb_form_filter.apply.dbal.text' => ['filterText'],
            'mustanggb_mongodb_form_filter.apply.dbal.email' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.integer' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.money' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.number' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.percent' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.search' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.url' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.choice' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.country' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.language' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.locale' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.timezone' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.date' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.dbal.datetime' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.dbal.birthday' => ['filterDate'],
            'mustanggb_mongodb_form_filter.apply.dbal.checkbox' => ['filterValue'],
            'mustanggb_mongodb_form_filter.apply.dbal.radio' => ['filterValue'],
        ];
    }
}
