<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Event;

/**
 * Former event class used to apply a condition on a query builder.
 *
 * @deprecated Use "MustangGB\Bundle\MongoDBFormFilterBundle\Event\GetFilterConditionEvent" instead
 */
class ApplyFilterEvent extends GetFilterConditionEvent
{
}
