<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Event;

/**
 * Available filter events.
 */
class FilterEvents
{
    public const PREPARE = 'mustanggb_filter.prepare';
}
