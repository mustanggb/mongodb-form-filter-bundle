UPGRADE FROM previous to 9.0
=============================

You need to search and replace all namespace in your code:

before

```php
Lexik\Bundle\FormFilterBundle
```

after

```php
MustangGB\Bundle\MongoDBFormFilterBundle
```


In `bundles.php`:

before

```php
use Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle;

...

LexikFormFilterBundle::class => ['all' => true],
```

after

```php
use MustangGB\Bundle\MongoDBFormFilterBundle\MustangGBMongoDBFormFilterBundle;

...

MustangGBMongoDBFormFilterBundle::class => ['all' => true],
```


In `lexik_form_filter.yaml` / `lexik_form_filter.php` bundle configuration file:

before

```php
lexik_form_filter
```

after

```php
mustanggb_mongodb_form_filter
```

