<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\Comment\HeaderCommentFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return static function (ECSConfig $config): void {
    $header = <<<EOF
EOF;

    $config->ruleWithConfiguration(HeaderCommentFixer::class, [
        'header' => $header
    ]);

    $config->ruleWithConfiguration(HeaderCommentFixer::class, [
        'header' => $header
    ]);

    $config->parallel();
    $config->paths([__DIR__]);
    $config->skip([
        __DIR__ . '/.github',
        __DIR__ . '/vendor',
    ]);
};
