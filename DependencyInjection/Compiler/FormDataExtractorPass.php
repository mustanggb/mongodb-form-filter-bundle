<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Add extraction methods to the data extraction service.
 */
class FormDataExtractorPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
        if ($container->hasDefinition('mustanggb_mongodb_form_filter.form_data_extractor')) {
            $definition = $container->getDefinition('mustanggb_mongodb_form_filter.form_data_extractor');

            foreach ($container->findTaggedServiceIds('mustanggb_mongodb_form_filter.data_extraction_method') as $id => $attributes) {
                $definition->addMethodCall('addMethod', [new Reference($id)]);
            }
        }
    }
}
