<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form;

use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\BooleanFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\CheckboxFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\CollectionAdapterFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateRangeFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateTimeFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateTimeRangeFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberRangeFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\SharedableFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractExtension;

/**
 * Load all filter types.
 */
class FilterExtension extends AbstractExtension
{
    /**
     * @return array
     */
    protected function loadTypes(): array
    {
        return [new BooleanFilterType(), new CheckboxFilterType(), new ChoiceFilterType(), new DateFilterType(), new DateRangeFilterType(), new DateTimeFilterType(), new DateTimeRangeFilterType(), new NumberFilterType(), new NumberRangeFilterType(), new TextFilterType(), new CollectionAdapterFilterType(), new SharedableFilterType()];
    }

    /**
     * {@inheritdoc}
     */
    public function loadTypeExtensions(): array
    {
        return [
            new FilterTypeExtension(),
        ];
    }
}
