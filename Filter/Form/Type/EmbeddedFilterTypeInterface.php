<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type;

/**
 * Some filter type can implement this interface to apply the filter to a property from an embedded object.
 * (Doctrine ORM)
 */
interface EmbeddedFilterTypeInterface
{
}
