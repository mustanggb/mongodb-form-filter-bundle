<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\DataExtractor\Method;

use Symfony\Component\Form\FormInterface;

/**
 * Extract data needed to apply a filter condition.
 */
class DefaultExtractionMethod implements DataExtractionMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'default';
    }

    /**
     * {@inheritdoc}
     */
    public function extract(FormInterface $form)
    {
        return ['value' => $form->getData()];
    }
}
