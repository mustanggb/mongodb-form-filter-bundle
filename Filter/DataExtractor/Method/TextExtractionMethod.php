<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\DataExtractor\Method;

use Symfony\Component\Form\FormInterface;

/**
 * Extract data needed to apply a filter condition.
 */
class TextExtractionMethod implements DataExtractionMethodInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function extract(FormInterface $form)
    {
        $data = $form->getData();
        $values = ['value' => []];

        if (array_key_exists('text', $data)) {
            $values = ['value' => $data['text']];
            $values += $data;
        }

        return $values;
    }
}
