<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\DataExtractor;

use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\DataExtractor\Method\DataExtractionMethodInterface;
use Symfony\Component\Form\FormInterface;

/**
 */
interface FormDataExtractorInterface
{
    /**
     * Add an extration method.
     *
     * @param DataExtractionMethodInterface $method
     */
    public function addMethod(DataExtractionMethodInterface $method);

    /**
     * Extract form data by using the given method.
     *
     * @param FormInterface $form
     * @param string        $methodName
     * @return array
     */
    public function extractData(FormInterface $form, $methodName);
}
