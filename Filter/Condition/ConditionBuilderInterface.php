<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Condition;

interface ConditionBuilderInterface
{
    /**
     * Create the root node.
     *
     * @param string $operator
     * @return ConditionNodeInterface
     */
    public function root($operator);

    /**
     * Add a condition to a node.
     *
     * @param ConditionInterface $condition
     */
    public function addCondition(ConditionInterface $condition);

    /**
     * Returns the root node.
     *
     * @return ConditionNodeInterface
     */
    public function getRoot();
}
