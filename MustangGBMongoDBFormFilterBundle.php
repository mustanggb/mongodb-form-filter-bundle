<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle;

use MustangGB\Bundle\MongoDBFormFilterBundle\DependencyInjection\Compiler\FormDataExtractorPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MustangGBMongoDBFormFilterBundle extends Bundle
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new FormDataExtractorPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 0);
    }
}
