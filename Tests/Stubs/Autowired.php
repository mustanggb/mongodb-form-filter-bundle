<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Tests\Stubs;

use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\FilterBuilderUpdaterInterface;

class Autowired
{
    private FilterBuilderUpdaterInterface $filterBuilderUpdater;

    public function __construct(
        FilterBuilderUpdaterInterface $filterBuilderUpdater
    ) {
        $this->filterBuilderUpdater = $filterBuilderUpdater;
    }

    public function getFilterBuilderUpdater(): FilterBuilderUpdaterInterface
    {
        return $this->filterBuilderUpdater;
    }
}
