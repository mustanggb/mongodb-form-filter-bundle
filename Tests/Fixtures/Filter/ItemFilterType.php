<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Tests\Fixtures\Filter;

use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\FilterOperands;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\BooleanFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\CheckboxFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\DateTimeFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\NumberFilterType;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form filter for tests.
 */
class ItemFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['with_selector']) {
            $builder->add('name', TextFilterType::class, ['apply_filter' => $options['disabled_name'] ? false : null]);
            $builder->add('position', NumberFilterType::class, ['condition_operator' => FilterOperands::OPERATOR_GREATER_THAN]);
        } else {
            $builder->add('name', TextFilterType::class, ['condition_pattern' => FilterOperands::OPERAND_SELECTOR]);
            $builder->add('position', NumberFilterType::class, ['condition_operator' => FilterOperands::OPERAND_SELECTOR]);
        }

        $builder->add('enabled', $options['checkbox'] ? CheckboxFilterType::class : BooleanFilterType::class);
        $builder->add('createdAt', $options['datetime'] ? DateTimeFilterType::class : DateFilterType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['with_selector' => false, 'checkbox' => false, 'datetime' => false, 'disabled_name' => false]);
    }
}
