<?php

namespace MustangGB\Bundle\MongoDBFormFilterBundle\Tests\Filter\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use MustangGB\Bundle\MongoDBFormFilterBundle\Filter\Doctrine\ORMQuery;
use PHPUnit\Framework\TestCase;

class ORMQueryTest extends TestCase
{
    public function testHasJoinAlias()
    {
        $emMock = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $emMock
            ->expects($this->any())
            ->method('getExpressionBuilder')
            ->will($this->returnValue(new Expr()));

        $qb = new QueryBuilder($emMock);
        $qb->from('Root', 'r');
        $qb->leftJoin('r.association1', 'a1');
        $qb->leftJoin('r.association2', 'a2');
        $qb->innerJoin('a2.association22', 'a22');

        $query = new ORMQuery($qb);

        $this->assertTrue($query->hasJoinAlias('a2'));
        $this->assertFalse($query->hasJoinAlias('a3'));
    }
}
